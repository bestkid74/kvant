<?php /* Template Name: Contacts */

get_header(); ?>

    <section class="contacts">
        <div class="container">
            <div class="contacts__content">
                <div class="contacts__content_address">
                    <div class="contacts__content_address_item">
                        <div class="icon">
                            <i class="fas fa-map-marker-alt"></i>
                        </div>
                        <h2>АДРЕС</h2>
                        <p>Украина, г. Киев,<br>Львовская 111-5 (центр)</p>
                    </div>
                    <div class="contacts__content_address_item">
                        <div class="icon">
                            <i class="fas fa-mobile-alt"></i>
                        </div>
                        <h2>АДРЕС</h2>
                        <p>Украина, г. Киев,<br>Львовская 111-5 (центр)</p>
                    </div>
                    <div class="contacts__content_address_item">
                        <div class="icon">
                            <i class="fas fa-envelope"></i>
                        </div>
                        <h2>АДРЕС</h2>
                        <p>Украина, г. Киев,<br>Львовская 111-5 (центр)</p>
                    </div>
                    <div class="contacts__content_address_item">
                        <div class="icon">
                            <i class="far fa-calendar-alt"></i>
                        </div>
                        <h2>АДРЕС</h2>
                        <p>Украина, г. Киев,<br>Львовская 111-5 (центр)</p>
                    </div>
                </div>
                <div class="contacts__content_form">
                    <h3>НАПИШИТЕ НАМ!</h3>
                    <form>
                        <div class="form-part-wrapper">
                            <div class="form-part">
                                <label><input type="text" placeholder="Ваше имя"></label>
                                <label><input type="text" placeholder="Ваш телефон"></label>
                            </div>
                            <div class="form-part">
                                <label><input type="text" placeholder="Фамилия"></label>
                                <label><input type="text" placeholder="E-mail"></label>
                            </div>
                            <div class="form-part">
                                <label>
                                    <textarea placeholder="Ваш вопрос"></textarea>
                                </label>
                            </div>
                        </div>
                        <input type="submit" value="Отправить">
                    </form>
                </div>
            </div>
        </div>
    </section>

<?php
get_footer();

