<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package kvant
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/fonts/fontawesome-free/css/all.css">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">

  <header>
    <div class="container">
      <div class="header__content">
        <div class="header__content_logo">
          <a href="/">
            <img src="<?php echo get_template_directory_uri(); ?>/img/_src/logo.png" class="img-responsive" alt="logo">
          </a>
        </div>
        <div class="header__content_main">
          <div class="header__content_main_menu">
            <div class="nav-contact">
              <ul class="nav">
                <li><a href="#">Home</a></li>
                <li class="divider"></li>
                <li><a href="#">Photo</a></li>
                <li class="divider"></li>
                <li><a href="#">Destinations</a></li>
                <li class="divider with-arrow"></li>
                <li><a href="#">Testimonials</a></li>
                <li class="divider"></li>
                <li><a href="#">News</a></li>
              </ul>
              <div class="contact">
                <a href="#"><i class="fab fa-viber"></i>&nbsp;viber</a>
                <a href="#"><i class="fas fa-envelope"></i>&nbsp;info@mail.com</a>
              </div>
            </div>
            <div class="lang-social">
              <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/_src/flag.jpg" alt="flag" class="img-responsive"></a>
              <ul class="social">
                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                <li><a href="#"><i class="fab fa-instagram"></i></a></li>
              </ul>
            </div>
          </div>
          <div class="header__content_main_search">
              <?php get_search_form(); ?>
          </div>
        </div>
      </div>
    </div>
  </header>

<!--	<a class="skip-link screen-reader-text" href="#content">--><?php //esc_html_e( 'Skip to content', 'kvant' ); ?><!--</a>-->
<!---->
<!--	<header id="masthead" class="site-header">-->
<!--		<div class="site-branding">-->
<!--			--><?php
//			the_custom_logo();
//			if ( is_front_page() && is_home() ) :
//				?>
<!--				<h1 class="site-title"><a href="--><?php //echo esc_url( home_url( '/' ) ); ?><!--" rel="home">--><?php //bloginfo( 'name' ); ?><!--</a></h1>-->
<!--				--><?php
//			else :
//				?>
<!--				<p class="site-title"><a href="--><?php //echo esc_url( home_url( '/' ) ); ?><!--" rel="home">--><?php //bloginfo( 'name' ); ?><!--</a></p>-->
<!--				--><?php
//			endif;
//			$kvant_description = get_bloginfo( 'description', 'display' );
//			if ( $kvant_description || is_customize_preview() ) :
//				?>
<!--				<p class="site-description">--><?php //echo $kvant_description; /* WPCS: xss ok. */ ?><!--</p>-->
<!--			--><?php //endif; ?>
<!--		</div>-->
<!---->
<!--		<nav id="site-navigation" class="main-navigation">-->
<!--			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">--><?php //esc_html_e( 'Primary Menu', 'kvant' ); ?><!--</button>-->
<!--			--><?php
//			wp_nav_menu( array(
//				'theme_location' => 'menu-1',
//				'menu_id'        => 'primary-menu',
//			) );
//			?>
<!--		</nav>-->
<!--	</header>-->

	<div id="content" class="site-content">
