<?php /* Template Name: Product */

get_header(); ?>

    <section class="product">
        <div class="container">
            <div class="product__content">
                <div class="product-images">
                    <div class="sw-wrapper">
                        <!-- Swiper -->
                        <div class="swiper-container gallery-top">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/_src/slide.png)"></div>
                                <div class="swiper-slide" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/_src/slide.png)"></div>
                                <div class="swiper-slide" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/_src/slide.png)"></div>
                                <div class="swiper-slide" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/_src/slide.png)"></div>
                                <div class="swiper-slide" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/_src/slide.png)"></div>
                                <div class="swiper-slide" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/_src/slide.png)"></div>
                                <div class="swiper-slide" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/_src/slide.png)"></div>
                                <div class="swiper-slide" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/_src/slide.png)"></div>
                                <div class="swiper-slide" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/_src/slide.png)"></div>
                                <div class="swiper-slide" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/_src/slide.png)"></div>
                            </div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next"></div>
                            <div class="swiper-button-prev"></div>
                        </div>
                        <div class="swiper-container gallery-thumbs">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/_src/slide.png)"></div>
                                <div class="swiper-slide" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/_src/slide.png)"></div>
                                <div class="swiper-slide" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/_src/slide.png)"></div>
                                <div class="swiper-slide" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/_src/slide.png)"></div>
                                <div class="swiper-slide" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/_src/slide.png)"></div>
                                <div class="swiper-slide" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/_src/slide.png)"></div>
                                <div class="swiper-slide" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/_src/slide.png)"></div>
                                <div class="swiper-slide" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/_src/slide.png)"></div>
                                <div class="swiper-slide" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/_src/slide.png)"></div>
                                <div class="swiper-slide" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/_src/slide.png)"></div>
                            </div>
                        </div>
                    </div>

                    <a href="#" class="make-order">Сделать заказ</a>
                </div>
                <div class="product-description">
                    <h2>Аппарат 123-01</h2>
                    <p>Комплекс рентгеновский диагностический КРД 50 в модификации КРД "INDIagraf" цифровой (на 2 рабочих места)</p>
                    <p>Универсальный рентгенографический комплекс КРД INDIagraf предназначен для проведения всех видов рентгенографических обследований в травматологических отделениях, в рентгеновских кабинетах лечебных учреждений общего назначения. Комплекс обеспечивает проведение обследования на поворотном штативе при вертикальном, горизонтальном положении, а также положении пациента под наклоном.</p>
                    <p>КРД ІNDIagraf может быть использован как:: безкабинный флюорограф - для проведения массовой флюорографии грудной клиниты пациента; рентгенографический аппарат, позволяющий проводить большинство рентгенографических исследований в положении стоя, лежа, сидя, включая обследования пациентов с потерей сознания.</p>
                    <p>Компактность комплекса делает его идеальным для установки в рентгеновских кабинетах никак большой площади.</p>

                    <div class="product-description__tabs">
                        <ul class="tabs">
                            <li>
                                <a href="#" data-tab="description" class="active">ОПИСАНИЕ</a>
                            </li>
                            <li>
                                <a href="#" data-tab="features">Характеристики</a>
                            </li>
                            <li>
                                <a href="#" data-tab="reviews">Отзывы</a>
                            </li>
                        </ul>

                        <div class="tabs-content">
                            <div class="tabs-content__item description active">
                                <p>description Комплекс рентгеновский диагностический КРД 50 в модификации КРД "INDIagraf" цифровой (на 2 рабочих места)</p>
                                <p>Универсальный рентгенографический комплекс КРД INDIagraf предназначен для проведения всех видов рентгенографических обследований в травматологических отделениях, в рентгеновских кабинетах лечебных учреждений общего назначения. Комплекс обеспечивает проведение обследования на поворотном штативе при вертикальном, горизонтальном положении, а также положении пациента под наклоном.</p>
                                <p>КРД ІNDIagraf может быть использован как:: безкабинный флюорограф - для проведения массовой флюорографии грудной клиниты пациента; рентгенографический аппарат, позволяющий проводить большинство рентгенографических исследований в положении стоя, лежа, сидя, включая обследования пациентов с потерей сознания.</p>
                                <p>Компактность комплекса делает его идеальным для установки в рентгеновских кабинетах никак большой площади.</p>
                            </div>
                            <div class="tabs-content__item features">
                                <p>features Комплекс рентгеновский диагностический КРД 50 в модификации КРД "INDIagraf" цифровой (на 2 рабочих места)</p>
                                <p>Универсальный рентгенографический комплекс КРД INDIagraf предназначен для проведения всех видов рентгенографических обследований в травматологических отделениях, в рентгеновских кабинетах лечебных учреждений общего назначения. Комплекс обеспечивает проведение обследования на поворотном штативе при вертикальном, горизонтальном положении, а также положении пациента под наклоном.</p>
                                <p>КРД ІNDIagraf может быть использован как:: безкабинный флюорограф - для проведения массовой флюорографии грудной клиниты пациента; рентгенографический аппарат, позволяющий проводить большинство рентгенографических исследований в положении стоя, лежа, сидя, включая обследования пациентов с потерей сознания.</p>
                                <p>Компактность комплекса делает его идеальным для установки в рентгеновских кабинетах никак большой площади.</p>
                            </div>
                            <div class="tabs-content__item reviews">
                                <p>reviews Комплекс рентгеновский диагностический КРД 50 в модификации КРД "INDIagraf" цифровой (на 2 рабочих места)</p>
                                <p>Универсальный рентгенографический комплекс КРД INDIagraf предназначен для проведения всех видов рентгенографических обследований в травматологических отделениях, в рентгеновских кабинетах лечебных учреждений общего назначения. Комплекс обеспечивает проведение обследования на поворотном штативе при вертикальном, горизонтальном положении, а также положении пациента под наклоном.</p>
                                <p>КРД ІNDIagraf может быть использован как:: безкабинный флюорограф - для проведения массовой флюорографии грудной клиниты пациента; рентгенографический аппарат, позволяющий проводить большинство рентгенографических исследований в положении стоя, лежа, сидя, включая обследования пациентов с потерей сознания.</p>
                                <p>Компактность комплекса делает его идеальным для установки в рентгеновских кабинетах никак большой площади.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php
get_footer();

