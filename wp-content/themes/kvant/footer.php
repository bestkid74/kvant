<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package kvant
 */

?>

	</div><!-- #content -->

<section class="bottom-section">
  <div class="container">
    <div class="bottom-section__content">
      <div class="bottom-carousel">
        <div class="carousel-wrapper owl-carousel">
          <div class="bottom-carousel__item">
            <div class="bottom-carousel__item_product">
              <img src="<?php echo get_template_directory_uri(); ?>/img/_src/product_img.jpg" alt="">
              <div class="product-descr">
                <h4>ИНФОРМАЦИЯ 2</h4>
                <p>Комплекс рентгеновский диагностический КРД 50 в модификации КРД "INDIagraf" цифровой (на 2 рабочих места)</p>
              </div>
            </div>
            <div class="bottom-carousel__item_product">
              <img src="<?php echo get_template_directory_uri(); ?>/img/_src/product_img.jpg" alt="">
              <div class="product-descr">
                <h4>ИНФОРМАЦИЯ 2</h4>
                <p>Комплекс рентгеновский диагностический КРД 50 в модификации КРД "INDIagraf" цифровой (на 2 рабочих места)</p>
              </div>
            </div>
          </div>
          <div class="bottom-carousel__item">
            <div class="bottom-carousel__item_product">
              <img src="<?php echo get_template_directory_uri(); ?>/img/_src/product_img.jpg" alt="">
              <div class="product-descr">
                <h4>ИНФОРМАЦИЯ 2</h4>
                <p>Комплекс рентгеновский диагностический КРД 50 в модификации КРД "INDIagraf" цифровой (на 2 рабочих места)</p>
              </div>
            </div>
            <div class="bottom-carousel__item_product">
              <img src="<?php echo get_template_directory_uri(); ?>/img/_src/product_img.jpg" alt="">
              <div class="product-descr">
                <h4>ИНФОРМАЦИЯ 2</h4>
                <p>Комплекс рентгеновский диагностический КРД 50 в модификации КРД "INDIagraf" цифровой (на 2 рабочих места)</p>
              </div>
            </div>
          </div>
          <div class="bottom-carousel__item">
            <div class="bottom-carousel__item_product">
              <img src="<?php echo get_template_directory_uri(); ?>/img/_src/product_img.jpg" alt="">
              <div class="product-descr">
                <h4>ИНФОРМАЦИЯ 2</h4>
                <p>Комплекс рентгеновский диагностический КРД 50 в модификации КРД "INDIagraf" цифровой (на 2 рабочих места)</p>
              </div>
            </div>
            <div class="bottom-carousel__item_product">
              <img src="<?php echo get_template_directory_uri(); ?>/img/_src/product_img.jpg" alt="">
              <div class="product-descr">
                <h4>ИНФОРМАЦИЯ 2</h4>
                <p>Комплекс рентгеновский диагностический КРД 50 в модификации КРД "INDIagraf" цифровой (на 2 рабочих места)</p>
              </div>
            </div>
          </div>
          <div class="bottom-carousel__item">
            <div class="bottom-carousel__item_product">
              <img src="<?php echo get_template_directory_uri(); ?>/img/_src/product_img.jpg" alt="">
              <div class="product-descr">
                <h4>ИНФОРМАЦИЯ 2</h4>
                <p>Комплекс рентгеновский диагностический КРД 50 в модификации КРД "INDIagraf" цифровой (на 2 рабочих места)</p>
              </div>
            </div>
            <div class="bottom-carousel__item_product">
              <img src="<?php echo get_template_directory_uri(); ?>/img/_src/product_img.jpg" alt="">
              <div class="product-descr">
                <h4>ИНФОРМАЦИЯ 2</h4>
                <p>Комплекс рентгеновский диагностический КРД 50 в модификации КРД "INDIagraf" цифровой (на 2 рабочих места)</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="question">
        <a href="#">ЗАДАТЬ ВОПРОС</a>
        <p>У вас возник вопрос?<br>Задайте его нашим спациалистам.</p>
        <div class="divider"></div>
        <p>Text</p>
      </div>
    </div>
  </div>
</section>

<footer>
  <div class="container">
    <div class="footer__content">
      <div class="footer__content_logo-menu">
        <a href="/" class="footer-logo">
          <img src="<?php echo get_template_directory_uri(); ?>/img/_src/logo_footer.png" alt="foter-logo" class="img-responsive">
        </a>
        <ul class="footer-menu">
          <li><a href="#">О предприятии</a></li>
          <li class="divider"></li>
          <li><a href="#">Новости</a></li>
          <li class="divider"></li>
          <li><a href="?page_id=10">Оборудование</a></li>
          <li class="divider"></li>
          <li><a href="#">Сервис</a></li>
          <li class="divider"></li>
          <li><a href="#">Статьи</a></li>
          <li class="divider"></li>
          <li><a href="#">Партнери</a></li>
          <li class="divider"></li>
          <li><a href="?page_id=12">Контакты</a></li>
          <li class="divider"></li>
          <li><a href="#">История</a></li>
        </ul>
      </div>
      <div class="footer__content_info">
        <div class="footer__content_info_item">
          <h2>КОНТАКТЫ</h2>
          <p>Комплекс рентгеновский диагностический КРД 50 в модификации КРД "INDIagraf" цифровой (на 2 рабочих места)</p>
        </div>
        <div class="footer__content_info_item">
          <h2>Информация</h2>
          <p>Комплекс рентгеновский диагностический КРД 50 в модификации КРД "INDIagraf" цифровой (на 2 рабочих места)</p>
        </div>
        <div class="footer__content_info_item">
          <h2>ИНФОРМАЦИЯ 2</h2>
          <p>Комплекс рентгеновский диагностический КРД 50 в модификации КРД "INDIagraf" цифровой (на 2 рабочих места)</p>
        </div>
      </div>
    </div>
  </div>
</footer>

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
